#!/usr/bin/env python2

from metrics import UncertaintyMetric
from ase.io import read 
import numpy as np 
images = '18.traj'
calc = 'amp.amp'
metric = UncertaintyMetric(images=images, calculator=calc, normalized=True) 

water = read("water.traj", index='-2:')
# fps1 = metric.get_trainingset_feature_vectors(elementwise=False)
fps1 = metric.get_trainingset_feature_vectors(elementwise=True)
# print(len(fps1['O']))
# fps2 = metric.get_target_feature_vectors(image=water, elementwise=False, 
# 	save_pickle=True)
fps2 = metric.get_target_feature_vectors(image=water, elementwise=True, 
	save_pickle=False)
# lls1 = metric.get_trainingset_last_layer_vectors(elementwise=False)
lls1 = metric.get_trainingset_last_layer_vectors(elementwise=True)
# lls2 = metric.get_target_last_layer_vectors(image=water, elementwise=False,
# 	save_pickle=True)
lls2 = metric.get_target_last_layer_vectors(image=water, elementwise=True,
	save_pickle=True)
# print(fps2[0]['H'])

# print(lls2[0])

confidence = metric.get_confidence_elementwise(fps1, fps2[-1],)
confidence2 = metric.get_confidence_elementwise(lls1, lls2[-1])
# print(confidence, )
print(confidence, confidence2)