"""ML Uncertainty metrics"""


__all__ = ['get_trainingset_feature_vectors', 'get_target_feature_vectors',
		   'get_target_last_layer_vectors', 'get_confidence', 
		   'get_confidence_elementwise', 'get_weight_multiplication',
           'get_trainingset_last_layer_vectors',]

__version__ = "0.1.0"
__author__ = "Cheng Zeng"
__email__ = "cheng_zeng1@brown.edu"
