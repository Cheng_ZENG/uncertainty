"""Uncertainty metric"""
from ase import Atoms
from amp import Amp
from amp.utilities import hash_images, check_images
from amp.stats.bootstrap import BootStrap

import numpy as np
import pickle
from sklearn import preprocessing
from sklearn.decomposition import PCA
from os.path import isfile


class UncertaintyMetric:
    """
    Construct various uncertainty metrics. Based on the vector space we
    are looking at and how the distances from each training image are
    weighted, we can define various types of metrics.

    Parameters
    ------------
    images : list or str
    List of ASE atoms objects with positions, symbols, energies, and
    forces in ASE format. This is the training set of data. This can
    also be the path to an ASE trajectory (.traj) or database (.db)
    file. Energies can be obtained from any reference, e.g. DFT
    calculations.

    calculator: Amp calculator
    Amp calculator, the machine learning model trained from the images

    normalized: boolean
    Normal feature vectors

    force: boolean
    Training force or not
    """

    def __init__(self, images=None, calculator=None, normalized=True,
                 force=True, label=None):
        if images is not None:
            self.images = images = hash_images(images, ordered=True)
            check_images(images, forces=force)
            elements = list(set(list(images.values())[0].get_chemical_symbols()))
            self.elements = elements
        else:
            self.images = images
        if calculator is not None:
            self.model = calculator
            self.calc = Amp.load(calculator, label=label)
        self.label = label
        self.normalized = normalized
        self.ErrorCalculated = False
        self.EnsembleWeights = False
        self.PCACalculated = False
        self.Scaled = False

    def get_confidence(self, v_train, v_image, method='min_n',
                       scaler='maxabs', weight=1., pca=False, 
                       n_components=None, order=2,  scale=1.,
                       error_based=False, ensemble_based=False):
        """Get the confidence level defined by distances between a point
        and a set of points (defined on the training set).v_train is a
        2-D list or numpy array, v_image is a 1-D list or array.

        Parameters
        ------------
        v_train : list or numpy array
        vectors as constructed from the training set.

        v_image: list or numpy array
        vector(s) as constructed from the target image. Note that at one time
        only the confidence of one image can be evaluated.

        scaler: str
        algorithm used to scale the raw data

        weight: float, list or array
        Weights for different features when computing the distances.

        pca: boolean
        For high dimensional data, a pca transformation is recommended before
        computing the distances.

        n_components: int
        Number of principal components kept in the PCA transformation

        order: int
        the order of the norm used to compute the distances

        scale: float
        scale factor for the confidence evaluation.

        error_based: bool
        if True, the error of the training images will be supplied as
        additional weights for the distances

        ensemble_based: bool
        if True, use the prediction variance for each image as the weights
        for distances

        Return
        --------
        The confidence level of the image of interest
        """

        v_train, v_image = np.array(v_train), np.array(v_image)
        size_train, size_image = v_train.shape, len(v_image)
        assert size_image == size_train[1]
        v = v_train
        if self.normalized:
            # [v_image] convert 1D array into 2D array,
            # a must for `tranform` method
            assert scaler in ['maxabs', 'minmax', 'standard']
            if self.Scaled:
                v_scaled = self.v_scaled
                v_image = [v_image]
                v_image = self.scaler.transform(v_image)
            else:
                if scaler == 'maxabs':
                    maxabs_scaler = preprocessing.MaxAbsScaler()
                    v_scaled = maxabs_scaler.fit_transform(v)
                    v_image = maxabs_scaler.transform([v_image])
                    self.scaler = maxabs_scaler
                elif scaler == 'minmax':
                    minmax_scaler = preprocessing.MinMaxScaler()
                    v_scaled = minmax_scaler.fit_transform(v)
                    v_image = minmax_scaler.transform([v_image])
                    self.scaler = minmax_scaler
                elif scaler == 'standard':
                    standard_scaler = preprocessing.StandardScaler().fit(v)
                    v_scaled = standard_scaler.transform(v)
                    v_image = standard_scaler.transform([v_image])
                    self.scaler = standard_scaler
            # self.Scaled = True
            self.v_scaled = v_scaled
        else:
            v_scaled = v
        if self.normalized and pca:
            assert isinstance(n_components, int)
            if self.PCACalculated:
                pca = self.pca
                v_scaled = self.pca_v_scaled
                v_image = pca.transform(v_image)[0]
            else:
                pca = PCA(n_components=n_components)
                v_scaled = pca.fit_transform(v_scaled)
                self.PCACalculated = True
                self.pca = pca
                self.pca_v_scaled = v_scaled
                v_image = pca.transform(v_image)[0]
        v_scaled = np.vstack([v_scaled, v_image])
        assert isinstance(order, int)
        v_dist = ((weight*(np.abs(v_scaled - v_scaled[-1])**order))
                  .sum(axis=1))**(1/order)
        v_dist = v_dist[:-1]
        if error_based:
            factor = self.get_prediction_error(forces=True)
        elif ensemble_based:
            factor = self.get_ensemble_var()
            # factor = factor/2.0
        else:
            factor = 1.0
        v_dist = factor*v_dist
        confidence = confidence_func(v_dist, scale=scale, method=method)
        return confidence

    def get_confidence_elementwise(self, v_train, v_target, method='min_n',
                                   weight=1.):
        """Get the elementwise confidence level for a given image. For
        the vector in each elementwise vector, the approach of computing
        each entry of confidence is the same as used in the `get_confidence`
        function.

        Parameters
        -------------
        v_train: dict
        dictionary consisting of elementwise vectors on the training set

        v_target: dict
        dictionary consisting of  elementwise vectors of the target image
        to be evaluted
        """

        assert isinstance(v_train, dict) and isinstance(v_target, dict)
        assert isinstance(weight, (float, dict))
        # unpack the elementwise vectors
        confidence = {}
        for element, vectors in v_train.iteritems():
            confidence[element] = []
            for each in v_target[element]:
                if isinstance(each, float) and hasattr(vectors, '__len__'):
                    vectors = np.array(vectors).reshape([len(vectors), -1])
                    each = [each]
                if isinstance(weight, dict):
                    weight = weight[element]
                    confidence_per = self.get_confidence(vectors, each,
                                                         method=method,
                                                         weight=weight)
                else:
                    confidence_per = self.get_confidence(vectors, each,
                                                         method=method)
                confidence[element].append(confidence_per)
        return confidence

    def get_trainingset_feature_vectors(self, save_pickle=True,
                                        elementwise=False):
        """Compute feature vectors of training set."""
        filename = "feature_space_vectors.pkl" if not elementwise \
            else "feature_space_vectors_elementwise.pkl"
        if isfile(filename):
            with open(filename, 'rb') as f:
                fp_train = pickle.load(f)
            return fp_train
        calc = self.calc
        images = self.images
        calc.descriptor.calculate_fingerprints(images)
        if not elementwise:
            fps_train = []
            for hash in images.keys():
                fingerprints = calc.descriptor.fingerprints[hash]
                fp = []
                for fingerprint in fingerprints:
                    fp += fingerprint[-1]
                fps_train.append(fp)
        else:
            elements = self.elements
            fps_train = {}
            for element in elements:
                fps_train[element] = []
            for hash in images.keys():
                fingerprints = calc.descriptor.fingerprints[hash]
                for fingerprint in fingerprints:
                    atom = fingerprint[0]
                    fps_train[atom].append(fingerprint[-1])

        if save_pickle:
            with open(filename, 'wb') as f:
                pickle.dump(fps_train, f)
        return fps_train

    def get_target_feature_vectors(self, image, elementwise=False,
                                   save_pickle=False):
        file = 'target_feature_space_vectors.pkl' if not elementwise \
                else 'target_feature_space_vectors_elementwise.pkl'
        if isfile(file):
            with open(file, 'rb') as f:
                fp_target = pickle.load(f)
            return fp_target
        calc = self.calc
        if isinstance(image, Atoms):
            atoms = [image, image]
            atoms = hash_images(atoms)
            calc.descriptor.calculate_fingerprints(atoms)
            hash_id = atoms.keys()[0]
            fp_image = calc.descriptor.fingerprints[hash_id]
            if not elementwise:
                fp_target = []
                for fingerprint_per in fp_image:
                    fp_target += fingerprint_per[-1]
            else:
                elements = self.elements
                fp_target = {}
                for element in elements:
                    fp_target[element] = []
                    for fingerprint_per in fp_image:
                        fp_target[element].append(fingerprint_per[-1])
        else:
            atoms = hash_images(image, ordered=True)
            calc.descriptor.calculate_fingerprints(atoms)
            if not elementwise:
                fp_target = []
                for hash in atoms.keys():
                    fingerprints = calc.descriptor.fingerprints[hash]
                    fp = []
                    for fingerprint in fingerprints:
                        fp += fingerprint[-1]
                    fp_target.append(fp)
            else:
                elements = self.elements
                fp_target = []
                for hash in atoms.keys():
                    fingerprints = calc.descriptor.fingerprints[hash]
                    fp = {}
                    for element in elements:
                        fp[element] = []
                    for fingerprint in fingerprints:
                        atom = fingerprint[0]
                        fp[atom].append(fingerprint[-1])
                    fp_target.append(fp)
            if save_pickle:
                with open(file, 'wb') as pf:
                    pickle.dump(fp_target, pf)

        return fp_target

    def get_trainingset_last_layer_vectors(self, save_pickle=True,
                                           elementwise=False):
        """ compuate the full-length or element-wise vectors
        on the training set in the last layer vector space,
        i.e. the atomic energies."""
        filename = 'last_layer_vectors.pkl' if not elementwise \
            else 'last_layer_vectors_elementwise.pkl'
        if isfile(filename):
            with open(filename, 'rb') as f:
                ll_train = pickle.load(f)
            return ll_train
        calc = self.calc
        images = self.images
        if not elementwise:
            ll_train = []  # last layer vectors
            for atoms in images.values():
                calc.get_potential_energy(atoms)
                atomic_energies = calc.model.atomic_energies
                ll_train.append(atomic_energies)
        else:
            elements = self.elements
            ll_train = {}
            for element in elements:
                ll_train[element] = []
            for atoms in images.values():
                symbols = atoms.get_chemical_symbols()
                calc.get_potential_energy(atoms)
                atomic_energies = calc.model.atomic_energies
                for symbol, ae in zip(symbols, atomic_energies):
                    ll_train[symbol].append(ae)

        if save_pickle:
            with open(filename, 'wb') as f:
                pickle.dump(ll_train, f)
        return ll_train

    def get_target_last_layer_vectors(self, image, elementwise=False,
                                      save_pickle=False):
        file = 'target_ll_vectors.pkl' if not elementwise \
                else 'target_ll_vectors_elementwise.pkl'
        if isfile(file):
            with open(file, 'rb') as f:
                ll_target = pickle.load(f)
            return ll_target
        calc = self.calc
        if isinstance(image, Atoms):
            calc.get_potential_energy(image)
            atomic_energies = calc.model.atomic_energies
            if not elementwise:
                ll_target = atomic_energies
            else:
                elements = self.elements
                ll_target = {}
                for element in elements:
                    ll_target[element] = []
                symbols = image.get_chemical_symbols()
                for symbol, ae in zip(symbols, atomic_energies):
                    ll_target[symbol].append(ae)
        else:
            image = hash_images(image, ordered=True)
            if not elementwise:
                ll_target = []
                for atoms in image.values():
                    calc.get_potential_energy(atoms)
                    atomic_energies = calc.model.atomic_energies
                    ll_target.append(atomic_energies)
            else:
                elements = self.elements
                ll_target = []
                for atoms in image.values():
                    ll = {}
                    for element in elements:
                        ll[element] = []
                    calc.get_potential_energy(atoms)
                    atomic_energies = calc.model.atomic_energies
                    symbols = atoms.get_chemical_symbols()
                    for symbol, ae in zip(symbols, atomic_energies):
                        ll[symbol].append(ae)
                    ll_target.append(ll)
        if save_pickle:
            with open(file, 'wb') as pf:
                pickle.dump(ll_target, pf)

        return ll_target

    def get_prediction_error(self, images=None, forces=False, coef=0.04,
                             ensemble=False):
        if self.ErrorCalculated:
            return self.errors
        else:
            images = self.images if images is None else \
            hash_images(images, ordered=True)
            calc = self.calc if not ensemble else \
            BootStrap(load=ensemble, label=self.label)
            errors = []
            for image in images.values():
                # note dft energy can also be emipiral potential energy
                dft = image.get_potential_energy()
                model = calc.get_potential_energy(image)
                error = abs(dft - model)
                if forces:
                    dft_forces = image.get_forces(apply_constraint=False)
                    model_forces = calc.get_forces(image)
                    error += coef*np.linalg.norm(dft_forces - model_forces,
                     ord="fro")
                errors.append(error)
            errors = np.array(errors)
            self.ErrorCalculated = True
            self.errors = errors
            return errors

    def get_ensemble_var(self, ensemble='bootstrap.ensemble'):
        '''Obtain the prediction variance by a trained ensemble model'''
        from sklearn.preprocessing import MinMaxScaler
        scaler = MinMaxScaler(feature_range=(0.5, 1.5))
        if self.EnsembleWeights:
            return self.vars
        if not isfile(ensemble):
            raise RuntimeError("Ensemble model not found")
        calc = BootStrap(load=ensemble, label=self.label)
        images = self.images
        vars = []
        for _ in images.values():
            energies = [model.get_potential_energy(_) for model in  
            calc.ensemble]
            vars.append(np.quantile(energies, 0.95) - np.quantile(energies, 0.05))
        vars = scaler.fit_transform(np.array(vars).reshape(-1,1))
        self.vars = vars
        self.EnsembleWeights = True
        return vars


    def get_dip(self, nbins=50, with_pca=True, n_components=10,
                normalized=True, save_pickle=False):
        """DIP: histogram density based proportion of features
        in the interpolation region.

        Parameters
        ------------
        nbins : int
        number of bins for density estimation

        with_pca: bool
        If the feature vectors are preprocessed with principal component
        analysis

        n_components: int
        Number of component kept for PCA analysis

        save_pickle: bool
        If the histogram density and bin edges will be saved

        Return
        --------
        The weighted proportions of features in the interpolation region
        for all target feature vectors.

        TODO: add kernel density estimation
        """
        from sklearn.preprocessing import MinMaxScaler
        nbins = int(nbins)
        fsv_pkl = "feature_space_vectors.pkl"
        tfsv_pkl = "target_feature_space_vectors.pkl"
        if not isfile(fsv_pkl) or not isfile(fsv_pkl):
            raise RuntimeError("Please get feature vector before dip")
        with open(fsv_pkl, "rb") as pf:
            fpv = pickle.load(pf)
        with open(tfsv_pkl, "rb") as pf:
            fpv_target = pickle.load(pf)
        fpv = np.array(fpv)
        fpv_target = np.array(fpv_target)
        # apply `MinMaxScaler` on the feature vectors of training images
        scaler = MinMaxScaler()
        fpv = scaler.fit_transform(fpv)
        fpv_target = scaler.transform(fpv_target)
        if with_pca:
            pca = PCA(n_components=n_components)
            fpv = pca.fit_transform(fpv)
            fpv_target = pca.transform(fpv_target)
            variances = pca.explained_variance_ratio_
        n_cols = fpv.shape[1]
        # Compute histogram densities and bin_edges of training images
        hist_density, bin_edges = [], []
        for k in range(n_cols):
            hist, edges = np.histogram(fpv[:, k], bins=nbins, density=True)
            hist_density.append(hist)
            bin_edges.append(edges)   
        if save_pickle:
            density_dict = {}
            density_dict['hist_density'] = hist_density     
            density_dict['bin_edges'] = bin_edges
            density_dict['scaler'] = scaler
            density_dict['pca'] = pca
            with open("density_training.pkl", "wb") as pf:
                pickle.dump(density_dict,pf)
        dip = []
        for i in range(fpv_target.shape[0]):
            density_weights = np.zeros(n_cols)
            target = fpv_target[i,:]
            for j in range(n_cols):
                hist_per = hist_density[j]
                edges_per = bin_edges[j][:-1]
                pos = (edges_per - target[j] < 0).sum()
                density_weights[j] = hist_per[pos-1]*variances[j]
            if normalized:
                hist_density = np.array(hist_density)
                dip.append(sum(density_weights)/
                    sum(hist_density.max(axis=1) * variances))
            else:
                dip.append((density_weights).mean())
        return np.array(dip)

def confidence_func(x, scale=1., method='mean'):
    """
    function to calculate the confidence of an image. It takes as input
    the (weighted) vector distances between a testing image and the training
    images.The default is the average of the confidence.
    The other two methods are

    - the max of the confidence

    - the average of the first n maximum confidence, default n is 20% of
    training images
    """
    assert method in ['mean', 'min', 'min_n']
    x = scale*np.array(x)
    if method == 'mean':
        return np.mean(1/(1+x))
    elif method == 'min':
        return np.max(1/(1+x))
    else:
        n = int(0.2*len(x))
        x = np.sort(x)[:n]
        return np.mean(1/(1+x))
