
from __future__ import print_function, division
import numpy as np 
from ase.build import molecule 

atoms = molecule('H2O')
elements = list(set(atoms.get_chemical_symbols()))

# dic = {}
# for element in elements:
# 	dic[element] = []

# print(dic)
a = np.array([[1,2,3], 
			  [3,5,7],
			  [3,11,17]])

b =  np.array([6,7,8])

print(b*a)
# c = np.vstack([a, b])

# d = ((c - c[-1])**2).sum(axis=1)

# d = d[:-1]
# # factor=2
# factor = [2,3,4,]

# func = None 
# f = lambda x: np.round(np.mean(1/(1+x)),3) if func is None else func 

# m = np.mean(1/(1+d))

# # print(m)
# # print(d, f(d))


# t = np.array([3,3,3])
# m = np.array([2,3])
# print (isinstance(t, np.ndarray))
# print(np.unique(t)[0])

# f = lambda x: np.max(x)

# print(d, f(d))

# print([1,2]+[3,4])

# a = [1,2,3]

# print(np.array(a).reshape([len(a), -1]))

# b = 1 

# print([b])

a = np.array([1,2])

# print(isinstance(a, np.ndarray))
print(np.exp(a))
# print(hasattr([b], '__len__'))